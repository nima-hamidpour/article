<?php

use Illuminate\Support\Facades\Route;

// AUTH ADMIN
Route::prefix('login')->group(function () {
    Route::get('/', 'AuthController@loginForm')->name('form-login');
    Route::post('/', 'AuthController@login')->name('login');
});

// AUTH ADMIN
Route::prefix('register')->group(function () {
    Route::get('/', 'AuthController@registerForm')->name('form-register');
    Route::post('/', 'AuthController@register')->name('register');
});

