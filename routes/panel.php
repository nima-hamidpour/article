<?php

use Illuminate\Support\Facades\Route;

Route::any('/logout', 'AuthController@logout')->name('logout');

Route::get('/', 'DashboardController@index')->name('dashboard');

//************ article *************
Route::prefix('article')->name('article.')->group(function () {
    Route::get('/list', 'ArticleController@index')->name('list');
    Route::get('/add', 'ArticleController@create')->name('create');
    Route::post('/store', 'ArticleController@store')->name('store');
    Route::get('/show/{id}', 'ArticleController@show')->name('show');
    Route::get('/edit/{id}', 'ArticleController@edit')->name('edit');
    Route::post('/update/{id}', 'ArticleController@update')->name('update');
    Route::get('/destroy/{id}', 'ArticleController@destroy')->name('destroy');
});
