<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// ********** DASHBOARD **********
Breadcrumbs::register('panel.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(' پنل کاربری', route('panel.dashboard'));
});

// ********** ARTICLE **********
Breadcrumbs::register('panel.article.list', function ($breadcrumbs) {
    $breadcrumbs->parent('panel.dashboard');
    $breadcrumbs->push('لیست مقالات', route('panel.article.list'));
});
Breadcrumbs::register('panel.article.create', function ($breadcrumbs) {
    $breadcrumbs->parent('panel.article.list');
    $breadcrumbs->push('افزودن مقاله جدید', route('panel.article.create'));
});
Breadcrumbs::register('panel.article.edit', function ($breadcrumbs, $article) {
    $breadcrumbs->parent('panel.article.list');
    $breadcrumbs->push('ویرایش مقاله' . $article->id, route('panel.article.edit', $article->id));
});
Breadcrumbs::register('panel.article.show', function ($breadcrumbs, $article) {
    $breadcrumbs->parent('panel.article.list');
    $breadcrumbs->push('مشاهده مقاله' . $article->id, route('panel.article.show', $article->id));
});
