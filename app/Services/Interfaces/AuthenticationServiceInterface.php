<?php
namespace App\Services\Interfaces;

interface AuthenticationServiceInterface
{
    public function register(array $data);
    public function login(array $data);
    public function logout();
}
