<?php
namespace App\Services\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface ArticleServiceInterface
{
    public function get();
    public function show($id);
    public function edit($article);
    public function store(array $data);
    public function update($article,array $data);
    public function destroy($article);

}
