<?php

namespace App\Services;

use App\Filters\ArticleFilter;
use App\Models\Article;
use App\Models\User;
use App\Services\Interfaces\UserServiceInterface;
use App\Traits\ConvertDateTrait;

class UserService implements UserServiceInterface
{

    use ConvertDateTrait;

    public function __construct(private ArticleFilter $filter)
    {
    }

    public function getAuthor()
    {
        return User::all();
    }

}
