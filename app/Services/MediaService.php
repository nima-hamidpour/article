<?php

namespace App\Services;

use App\Filters\ArticleFilter;
use App\Models\Article;
use App\Services\Interfaces\ArticleServiceInterface;
use App\Services\Interfaces\MediaServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Traits\ConvertDateTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;

class MediaService implements MediaServiceInterface
{

    public function upload(Model $model, $file)
    {
        return $model
            ->addMedia($file)
            ->toMediaCollection();
    }

}
