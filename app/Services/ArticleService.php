<?php

namespace App\Services;

use App\Filters\ArticleFilter;
use App\Models\Article;
use App\Services\Interfaces\ArticleServiceInterface;
use App\Services\Interfaces\MediaServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Traits\ConvertDateTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;
use Throwable;

class ArticleService implements ArticleServiceInterface
{

    use ConvertDateTrait;

    public function __construct(private ArticleFilter $filter,
    private UserServiceInterface $userService,
    private MediaServiceInterface $mediaService
    )
    {
    }

    public function get()
    {
        return Article::filter($this->filter)->paginate();
    }

    /**
     * @param $id
     * @return Article|Article[]|Collection|Model
     */
    public function show($id): Model|Article|Collection|array
    {
        return Article::with('author')->findOrFail($id);
    }

    /**
     * @param array $data
     * @return RedirectResponse
     * @throws Throwable
     */
    public function store(array $data): RedirectResponse
    {
        try {
          DB::beginTransaction();

            $article =  Article::create([
                'author'           => Auth::id(),
                'title'            => $data['title'],
                'content'          => $data['content'],
                'publication_date' => $data['publication_date'],
            ]);
            $this->mediaService->upload($article,$data['image']);
            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            return redirect()->back()->with('warning', __('panel.failed'));
        }
        return redirect()->route('panel.article.list');
    }

    /**
     * @param $article
     * @return array
     */
    public function edit($article): array
    {
        $response['article'] =  $article;
        $response['author'] = $this->userService->getAuthor();
        return  $response;
    }

    /**
     * @param $article
     * @param array $data
     * @return RedirectResponse
     * @throws Throwable
     */
    public function update($article, array $data): RedirectResponse
    {
        try {
            DB::beginTransaction();
            $article->update($data);
            if (isset($data['image'])){
                $this->mediaService->upload($article,$data['image']);
            }
            DB::commit();
        } catch (Throwable $throwable) {dd($throwable);
            DB::rollBack();
            return redirect()->back()->with('warning', __('panel.failed'));
        }
        return redirect()->route('panel.article.list');
    }

    public function destroy($article)
    {
        try {
            $article->delete();
        } catch (Throwable $throwable) {
            return redirect()->back()->with('warning', __('panel.failed'));
        }
        return redirect()->route('panel.article.list');
    }
}
