<?php

namespace App\Services;

use App\Models\User;
use App\Services\Interfaces\AuthenticationServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class AuthenticationService implements AuthenticationServiceInterface
{
    /**
     * @param array $data
     * @return RedirectResponse
     */
    public function register(array $data): RedirectResponse
    {

        try {
            $user = User::create($data);
            Auth::login($user);
        } catch (\Throwable $throwable) {
            redirect()->back()->with('warning', __('panel.failed'));
        }
        return redirect()->route('panel.dashboard');
    }

    /**
     * @param array $data
     * @return RedirectResponse
     */
    public function login(array $data): RedirectResponse
    {
        if (!Auth::guard()->attempt(['mobile' => $data['mobile'], 'password' => $data['password']])) {
            return redirect()->back()->with('warning', __('panel.wrong_username_or_password'));
        }
        return redirect()->route('panel.dashboard');
    }

    public function logout(): void
    {
        Auth::logout();
    }
}
