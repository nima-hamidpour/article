<?php

namespace App\Models;

use App\Traits\ConvertDateTrait;
use App\Traits\FilterableTrait;
use Attribute;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Morilog\Jalali\Jalalian;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\Article
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $author
 * @property string $publication_date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article filter(\App\Filters\Filters $filters)
 * @mixin Eloquent
 */
class Article extends Model implements HasMedia
{
    use FilterableTrait;
    use ConvertDateTrait;
    use  SoftDeletes;
    use InteractsWithMedia;


    protected $fillable = ['title','content','author','publication_date'];


    public function getPublicationDateAttribute($value): string
    {
        return $this->jalaliDate(Carbon::createFromTimeString($value));
    }

    public function setPublicationDateAttribute($value)
    {
        $this->attributes['publication_date'] = $this->gregorianDate(Jalalian::fromFormat('Y-m-d', $value));
    }

    public function getImageAttribute()
    {
        return  $this->getMedia()?->last()?->getUrl();
    }

    public function Author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author', 'id');
    }

}
