<?php

namespace App\Filters;

use App\Traits\ConvertDateTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\HigherOrderWhenProxy;
use Illuminate\Support\Str;

abstract class Filters
{
    use ConvertDateTrait;

    protected array $defaultOrderBy = [
        'created_at' => 'desc'
    ];

    protected array $validOrderBy = [
        'created_at' => 'desc'
    ];

    protected Request $request;
    protected Builder $builder;
    protected array $customFilters = [];


    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->getCustomFilters() as $customFilter => $customFilterValue) {
            $method = $customFilter;
            if (method_exists($this, $method = Str::camel($method))) {
                if (!is_array($customFilterValue)){
                    $customFilterValue =  escapeLike($customFilterValue);
                }
                if (!is_array($customFilterValue) &&
                    (str_contains($customFilter, 'id') ||  is_numeric($customFilterValue))){
                    $customFilterValue = convertPersianNumberToEnglish($customFilterValue);
                }
                $this->$method($customFilterValue);
            }
        }

        if ($this->request->filled('orderBy')) {
            $this->setDefaultOrder($this->request->orderBy);
        } elseif (property_exists($this, 'validOrderBy')) {
            $this->setDefaultOrder();
        }
        $this->orderBy($this->defaultOrderBy);
        return $this->builder;
    }


    /**
     * Fetch all relevant filters from the request.
     *
     * @return array
     */
    public function getCustomFilters(): array
    {
        return array_filter($this->request->only($this->customFilters), function ($item) {
            return !is_null($item);
        });
    }


    /**
     * @param $orders
     * @return Builder|HigherOrderWhenProxy|mixed
     */
    protected function orderBy($orders)
    {
        if (!is_array($orders)) {
            $orders = json_decode($orders, true);
        }
        return $this->builder->when(!empty($orders), function ($query) use ($orders) {
            foreach ($orders as $key => $order) {
                if (array_key_exists($key, $this->validOrderBy)) {
                    $query->orderBy($key, $order);
                }
            }
        });
    }

    public function setDefaultOrder(string|array $orderBy = null): void
    {
        if (!is_array($orderBy)) {
            $orderBy = json_decode($orderBy, true);
        }
        $this->defaultOrderBy[$this->builder->getModel()->getKeyName()] = 'desc';
        $this->validOrderBy[$this->builder->getModel()->getKeyName()] = 'desc';
        unset($this->defaultOrderBy['created_at'], $this->validOrderBy['created_at']);
        if (isset($orderBy['created_at']))
            unset($orderBy['created_at']);
        if (is_array($orderBy)){
            $this->defaultOrderBy = array_merge($this->defaultOrderBy, $orderBy);
            $this->validOrderBy = array_merge($this->validOrderBy, $orderBy);
        }
    }

}
