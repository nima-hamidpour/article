<?php

namespace App\Filters;

class ArticleFilter extends Filters
{

    protected array $defaultOrderBy = [
        'id'
    ];


    protected array $customFilters = [
        'title','content','author','publication_date'
    ];


    public function title($value)
    {
        return $this->builder->where('title','like','%'. $value . '%');

    }

    public function content($value)
    {
        return $this->builder->where('content','like','%'. $value . '%');

    }

    public function author($value)
    {
        return $this->builder->where('author',$value);
    }

    public function publicationDate($value)
    {
        return $this->builder->whereDate('publication_date',$value);
    }
}
