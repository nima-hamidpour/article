<?php

namespace App\Traits;

use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

trait ConvertDateTrait
{
    /**
     * @param Carbon      $date      Date.
     * @param string|null $separator Separator.
     * @return string
     */
    public function jalaliDate(Carbon $date, string $separator = null): string
    {
        $jalali =  jdate($date)->format('Y-m-d');
        return empty($separator) ? $jalali : str_replace('/', $separator, $jalali);
    }

    /**
     * @param Jalalian|null $date Date.
     * @return string
     */
    public function gregorianDate(?Jalalian $date): string
    {
        return $date->toCarbon()->toDateString();
    }

    /**
     * @param Carbon|string|null $dateTime DateTime.
     * @param string $format
     * @return string
     */
    public function jalaliDateTime(null|Carbon|string $dateTime, $format = 'Y-m-d H:i:s'): string
    {
        return jdate($dateTime)->format($format);
    }

    /**
     * @param Jalalian|null $dateTime DateTime.
     * @return string
     */
    public function gregorianDateTime(?Jalalian $dateTime): string
    {
        return $dateTime->toCarbon()->toDateString();
    }

    /**
     * @param string $date Date.
     * @return boolean
     */
    public function validateJalaliDate(string $date): bool
    {
        if (!empty($date) && preg_match('/(1)[0-4][0-9]{2}-[0-9]{1,2}-[0-9]{1,2}/', $date)) {
            return true;
        }
        return  false;
    }
}
