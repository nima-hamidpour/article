<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LoginRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'mobile'    => ['required',Rule::exists('users','mobile')],
            'password' => ['required','min:6']
        ];
    }

}
