<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'name'     => ['required', 'string'],
            'mobile'   => ['required', Rule::unique('users', 'mobile'), 'digits:11'],
            'password' => ['required', 'confirmed', 'min:6']
        ];
    }

}
