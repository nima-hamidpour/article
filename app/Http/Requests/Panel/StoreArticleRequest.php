<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreArticleRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'title'            => ['required', 'string'],
            'content'          => ['required', 'string'],
            'publication_date' => ['required', 'date_format:Y-m-d'],
            'image'            => ['required', 'file'],
        ];
    }

}
