<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateArticleRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'title'            => ['required', 'string'],
            'content'          => ['required', 'string'],
            'author'           => ['required', Rule::exists('users','id')],
            'publication_date' => ['required', 'date_format:Y-m-d'],
            'image'            => ['nullable', 'file'],

        ];
    }

}
