<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\StoreArticleRequest;
use App\Http\Requests\Panel\UpdateArticleRequest;
use App\Models\Article;
use App\Services\Interfaces\ArticleServiceInterface;

class ArticleController extends Controller
{

    public function __construct(private ArticleServiceInterface $articleService)
    {
    }

    public function index()
    {
        $response['article'] = $this->articleService->get();
        return view('panel.page.article.list', compact('response'));
    }

    public function show($articleId)
    {
        $response['article'] = $this->articleService->show($articleId);
        return view('panel.page.article.show',compact('response'));
    }

    public function create()
    {
        return view('panel.page.article.add');
    }

    public function store(StoreArticleRequest $articleRequest)
    {
        return $this->articleService->store($articleRequest->validated());
    }

    public function edit($articleId)
    {
        $article = Article::findOrFail($articleId);
        $this->authorize('update', Article::findOrFail($articleId));
        $response = $this->articleService->edit($article);
        return view('panel.page.article.edit',compact('response'));
    }

    public function update($articleId, UpdateArticleRequest $articleRequest)
    {
        $article = Article::findOrFail($articleId);
        $this->authorize('update', Article::findOrFail($articleId));
        return $this->articleService->update($article,$articleRequest->validated());
    }

    public function destroy($articleId)
    {
        $article = Article::findOrFail($articleId);
        $this->authorize('destroy',$article);
        return $this->articleService->destroy($article);
    }
}
