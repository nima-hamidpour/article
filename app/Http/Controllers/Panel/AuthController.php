<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Services\Interfaces\AuthenticationServiceInterface;

class AuthController extends Controller
{

    public function __construct(private AuthenticationServiceInterface $authenticationService)
    {
    }

    public function logout()
    {
        $this->authenticationService->logout();
        return redirect()->route('form-login');
    }
}
