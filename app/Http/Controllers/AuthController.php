<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\Interfaces\AuthenticationServiceInterface;

class AuthController extends Controller
{

    public function __construct(private AuthenticationServiceInterface $authenticationService)
    {
    }


    public function registerForm()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $registerRequest)
    {
        return $this->authenticationService->register($registerRequest->validated());
    }


    public function loginForm()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $loginRequest)
    {
        return $this->authenticationService->login($loginRequest->validated());
    }

}
