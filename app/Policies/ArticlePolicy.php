<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;

class ArticlePolicy
{

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function update(User $user,Article $article): bool
    {
        return $user->id == $article->author;
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function destroy(User $user,Article $article): bool
    {
        return $user->id == $article->author;
    }
}
