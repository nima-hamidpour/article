<?php

namespace App\Providers;

use App\Services\AuthenticationService;
use App\Services\ArticleService;
use App\Services\Interfaces\ArticleServiceInterface;
use App\Services\Interfaces\AuthenticationServiceInterface;
use App\Services\Interfaces\MediaServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Services\MediaService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class ServiceLayerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ArticleServiceInterface::class, ArticleService::class);
        $this->app->bind(AuthenticationServiceInterface::class, AuthenticationService::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(MediaServiceInterface::class, MediaService::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
