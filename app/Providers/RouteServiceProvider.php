<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{

    protected $namespace = 'App\\Http\\Controllers';

    public function boot(): void
    {
        $this->routes(function () {
            Route::middleware('web')
                ->namespace(sprintf('%s', $this->namespace))
                ->group(base_path('routes/web.php'));

            Route::prefix('panel')
                ->middleware(['web','auth:web'])
                ->namespace(sprintf('%s\%s', $this->namespace, 'Panel'))
                ->name('panel.')
                ->group(base_path('routes/panel.php'));
        });


    }
}
