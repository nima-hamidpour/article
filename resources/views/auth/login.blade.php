<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        ورود به پنل کاربری
    </title>
    <link rel="stylesheet" href="{{url('/css/app.css')}}">
    <link rel="stylesheet" href="{{url('/css/fontawesome/css/all.min.css ')}}">
</head>
<body class="bg-admin pt-5 mt-5">

<div class="container h-100">
    @include('message.message')

    <div class="d-flex justify-content-center h-100 mt-5">
        <div class="admin_card">

            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <img src="{{url(('img/logo1.png'))}}" class="brand_logo" alt="Logo">
                </div>
            </div>

            <div class="justify-content-center mt-5 px-3">
                <form method="POST" action="{{ route('login') }}">
                @csrf
                <!-- USER NAME -->
                    <div class="form-row">
                        <div class="col-12 text-right">
                            <label for="mobile" class="font-size-13 font-weight-bold">
                                نام کاربری
                            </label>
                            <input type="tel" name="mobile" id="mobile"
                                   class="form-control input_user    text-right"
                                   placeholder="نام کاربری">
                        </div>
                    </div>

                    <!-- PASSWORD -->
                    <div class="form-row mt-3">
                        <div class="col-12 text-right">
                            <label for="password" class="  font-size-13 font-weight-bold">
                                کلمه عبور
                            </label>
                            <input type="password" name="password"
                                   class="form-control input_pass  text-right"
                                   placeholder="کلمه عبور">
                        </div>
                    </div>
                    <!-- LOGIN -->
                    <div class="d-flex justify-content-center mt-3">
                        <button type="submit" class="btn login_btn">
                            ورود
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{url('/js/app.js')}}"></script>
</body>
</html>

