<!-- Sidebar -->
<ul class="sidebar navbar-nav p-1 m-0 sidebar-width sidebar-background">


    <!-- ADMIN INFORMATION -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-white menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user arrow-left mt-3 text-white"></i>
            <span class="  font-size-12 text-white font-weight-bold">
                     {{\Illuminate\Support\Facades\Auth::user()->name}}
            </span>

        </a>
    </li>


    <!-- DASHBOARD -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-white menu-padding sidebar-width" href="{{route('panel.dashboard')}}">
            <i class="fas fa-home text-white"></i>
            <span class="  font-size-15">داشبورد</span>
        </a>
    </li>


    <!-- ARTICLE -->
    <li class="nav-item border-bottom pb-1">
        <a class="nav-link text-right text-white menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-blog text-white"></i>
            <span class="  font-size-14">
                مقالات
            </span>
        </a>
        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('panel.article.list')}}" class="nav-link text-right text-white">
                <i class="fas fa-list text-dark"></i>
                <span class="text-dark font-size-12   font-weight-bold">لیست مقالات</span>
            </a>
            <a href="{{route('panel.article.create')}}" class="nav-link text-right text-white">
                <i class="fa fa-plus text-dark"></i>
                <span class="text-dark font-size-12   font-weight-bold">افزودن مقاله جدید</span>
            </a>
        </div>
    </li>



</ul>

