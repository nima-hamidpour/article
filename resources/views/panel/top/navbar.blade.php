<nav class="navbar navbar-expand navbar-dark static-top direction-rtl admin-navbar-background">
    <button class="btn btn-link btn-sm text-white order-1 order-sm-0 " id="sidebarToggle">
        <i class="fas fa-bars text-dark"></i>
    </button>
    <a class="navbar-brand mr-1 text-dark font-size-15  " href="{{route('panel.dashboard')}}">
      پنل کاربری
    </a>&nbsp;&nbsp;

    <!-- Navbar -->
    <ul class="navbar-nav mr-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-sign-out-alt text-dark"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
                <a class="dropdown-item text-center" href="#" data-toggle="modal" data-target="#logoutModal">خروج</a>
            </div>
        </li>
    </ul>

</nav>
<div id="mainNav"></div>
