@extends('panel.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('panel.article.create') }}
@endsection

@section('content')

    <section class="direction-ltr bg-white pt-0" id="welcome">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('panel.article.store')}}" method="POST"
                  enctype="multipart/form-data"
                  class="form admin-form">
                @csrf

                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{url('img/logo1.png')}}" id="previewImage"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="image" id="photo" onchange="readURL(this);"
                                   class="d-none">

                            <div class="d-inline-block" id="preview-img"></div>
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-md-6 mb-4">
                        <label for="title_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i> عنوان مطلب </label>
                        <input type="text" name="title" id="title_id" value="{{old('title')}}"
                               class="form-control admin-input" placeholder="عنوان مطلب">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="publication_date" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i> تاریخ انتشار </label>
                        <input type="text" name="publication_date" id="publication_date" value="{{old('publication_date')}}"
                               class="form-control admin-input" placeholder="1402-06-05">
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="content_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp; محتوا</label>
                        <textarea class="form-control admin-input p-2 content" id="content_id" name="content"
                                  placeholder="محتوا">{{old('content')}}</textarea>
                    </div>
                </div>


                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ثبت مطلب">
                </div>
            </form>
        </div>
    </section>
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#previewImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
