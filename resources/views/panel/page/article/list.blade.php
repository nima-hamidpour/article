@extends('panel.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('panel.article.list') }}
@endsection

@section('content')

    @include('message.message')

        <div class="row text-right">
            <div class="col-12 px-4">
                <a class="text-decoration-none btn btn-info text-right
                        btn-sm   font-size-14"
                   href="{{route('panel.article.create')}}">
                    افزودن مقاله جدید
                </a>
            </div>
        </div>

    <div class="table-responsive admin-div-table table-bordered mt-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-14">عنوان</td>
                <td class="font-size-14">عملیات</td>
            </tr>
            <tbody>
            @foreach($response['article'] as $article)
                <tr class="text-center font-size-13">
                    <td class="admin-info pt-4">{{$article->title}}</td>

                    <td>
                        <a href="{{route('panel.article.show',$article->id)}}" class="btn btn-outline-info btn-sm admin-a">
                            مشاهده
                        </a>
                        <a href="{{route('panel.article.edit',$article->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                            ویرایش
                        </a>
                        <a href="{{route('panel.article.destroy',$article->id)}}"
                           class="btn btn-outline-danger btn-sm admin-a">
                            حذف
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['article']->links()}}
    </div>

@endsection
