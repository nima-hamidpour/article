@extends('panel.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('panel.article.show',$response['article']) }}
@endsection

@section('content')

    <section class="bg-white pt-0" id="welcome">
        <div class="container-fluid  direction-rtl text-right my-5">
            <div class="row">
                <div class="col-12">
                    <img src="{{$response['article']->image}}">
                </div>
            </div>
         <div class="row">
             <div class="col-12">
                 عنوان مقاله :
                 {{$response['article']->title}}
             </div>
         </div>
            <div class="row">
                <div class="col-12">
                    توضیحات :
                {{$response['article']->content}}
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    نویسنده :
                    {{$response['article']->Author->name}}
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    تاریخ انتشار :
                    {{$response['article']->publication_date}}
                </div>
            </div>
        </div>
    </section>
@endsection
