FROM php:8.2-fpm-alpine

WORKDIR /var/www

RUN apk add --update --no-cache \
    build-base \
    libpng-dev \
    libjpeg-turbo-dev \
    freetype-dev \
    libzip-dev \
    icu-dev \
    vim \
    unzip \
    git \
    curl \
    jpegoptim \
    optipng \
    pngquant \
    gifsicle \
    redis \
    libexif-dev

RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-configure zip

RUN docker-php-ext-install pdo pdo_mysql exif

RUN rm -rf /var/cache/apk/*

COPY --from=composer:2.4 /usr/bin/composer /usr/bin/composer

EXPOSE 9000

ENTRYPOINT ["./docker/entrypoint-app.sh"]
