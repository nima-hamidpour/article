<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'نیما حمیدپور',
            'mobile' => '09133390765',
            'password' => Hash::make('123456'),
        ]);
    }
}
