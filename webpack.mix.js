const mix = require('laravel-mix');

mix.styles([
    'resources/css/bootstrap/bootstrap.css',
    'resources/css/bootstrap/bootstrap-grid.min.css',
    'resources/css/bootstrap/sb-admin.min.css',
    'resources/css/google/font-google-api.css',
    'resources/css/google/font-google-api2.css',
    'resources/css/style/basic.css',
    'resources/css/style/admin.css',
    'resources/css/persianDatePicker/persian-datepicker.css',
],'public/css/app.css');

mix.scripts([
    'resources/js/vendor/jquery/jquery.min.js',
    'resources/js/vendor/bootstrap/js/bootstrap.bundle.min.js',
    'resources/js/vendor/jquery-easing/jquery.easing.min.js',
    'resources/js/vendor/magnific-popup/jquery.magnific-popup.min.js',
    'resources/js/admin/sb-admin.min.js',
    'resources/js/other/sweetalert.js',
    'resources/js/persianDatePicker/persian-date.js',
    'resources/js/persianDatePicker/persian-datepicker.js',
],'public/js/app.js');
